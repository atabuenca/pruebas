package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import clases.GestorContabilidad;
import clases.Factura;
import clases.Cliente;

class TestMetodosFacturasGestor {
	
	private static GestorContabilidad gestor;
	private static Cliente cliente1;
	private static Cliente cliente2;
	private static Cliente cliente3;
	private static Factura factura1;
	private static Factura factura2;
	private static Factura factura3;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception{
		gestor=new GestorContabilidad();
		cliente1=new Cliente("Francisco", "123", LocalDate.parse("1990-08-08"));
		cliente2=new Cliente("Jos�", "456", LocalDate.parse("1990-07-07"));
		cliente3=new Cliente("Ramiro", "789", LocalDate.parse("1990-06-06"));
		factura1=new Factura("A", LocalDate.parse("1995-12-12"),"Carne", 5, 3, cliente1);
		factura2=new Factura("B", LocalDate.parse("1995-11-11"),"Pescado", 7, 3, cliente2);
		factura3=new Factura("C", LocalDate.parse("1995-10-10"),"Verdura", 3, 3, cliente3);
		gestor.crearFactura(factura1);
		gestor.crearFactura(factura2);
		gestor.crearFactura(factura3);
	}
	
	// Comprobamos el m�todo buscarFactura(String codigo) de la clase GestorContabilidad, que devuelve de la lista de facturas la factura con el c�digo pasado, si es que la hay.
	@Test
	@Tag("Factura")
	@Tag("B�squeda")
	void testBuscarFacturaExistente() {
		Factura facturaBuscada=gestor.buscarFactura("B");
		assertEquals(factura2, facturaBuscada);
	}
	
	// Comprobamos el m�todo buscarFactura(String codigo) de la clase GestorContabilidad, que devuelve de la lista de facturas la factura con el c�digo pasado, si es que la hay, en un caso inexistente.
	@Test
	@Tag("Factura")
	@Tag("B�squeda")
	void testBuscarFacturaInexistente() {
		Factura facturaBuscada=gestor.buscarFactura("D");
		assertNull(facturaBuscada);
	}
	
	// Comprobamos el m�todo facturaMasCara() de la clase GestorContabilidad, que devuelve la factura cuyo precio total es el m�s caro, dado un caso positivo.
	@Test
	@Tag("Factura")
	@Tag("Busqueda")
	void testFacturaMasCara() {
		Factura facturaMasCara=gestor.facturaMasCara();
		assertEquals(factura2, facturaMasCara);
	}

	// Comprobamos el m�todo facturaMasCara() de la clase GestorContabilidad, que devuelve la factura cuyo precio total es el m�s caro, dado un caso negativo.
	@Test
	@Tag("Factura")
	@Tag("Busqueda")
	void testFacturaMasCaraErronea() {
		Factura facturaMasCara=gestor.facturaMasCara();
		assertNotEquals(factura3, facturaMasCara);
	}
	
	// Comprobamos el m�todo calcularFacturacionAnual de la clase GestorContabilidad, que nos devuelve la facturaci�n dado un a�o en un caso correcto.
	@Test
	@Tag("Factura")
	@Tag("Matem�tico")
	void testCalcularFacturacionAnual() {
		float facturacion1995=gestor.calcularFacturacionAnual(1995);
		assertEquals(45, facturacion1995);
	}
	
	// Comprobamos el m�todo calcularFacturacionAnual de la clase GestorContabilidad, que nos devuelve la facturaci�n dado un a�o en un caso erroneo.
	@Test
	@Tag("Factura")
	@Tag("Matem�tico")
	void testCalcularFacturacionAnualErronea() {
		float facturacion1995=gestor.calcularFacturacionAnual(1995);
		assertNotEquals(40, facturacion1995);
	}

	// Comprobamos el m�todo eliminarFactura() de la clase GestorContabilidad, el cual elimina una factura del gestor dado el c�digo de factura.
	@Test
	@Tag("Factura")
	@Tag("Eliminaci�n")
	void testEliminarFactura() {
		gestor.eliminarFactura("B");
		Factura comprobacion=gestor.buscarFactura("B");
		assertNull(comprobacion);
	}
	
	
}
