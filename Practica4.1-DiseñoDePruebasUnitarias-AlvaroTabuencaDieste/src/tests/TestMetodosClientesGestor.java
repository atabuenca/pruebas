package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.GestorContabilidad;

class TestMetodosClientesGestor {
	
	private static GestorContabilidad gestor;
	private static Cliente cliente1;
	private static Cliente cliente2;
	private static Cliente cliente3;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		gestor=new GestorContabilidad();
		cliente1=new Cliente("Francisco", "123", LocalDate.parse("1990-08-08"));
		cliente2=new Cliente("Jos�", "456", LocalDate.parse("1990-07-07"));
		cliente3=new Cliente("Ramiro", "789", LocalDate.parse("1990-06-06"));
		gestor.altaCliente(cliente1);
		gestor.altaCliente(cliente2);
		gestor.altaCliente(cliente3);
	}

	// Comprobamos el m�todo buscarCliente(String dni) de la clase GestorContabilidad, que devuelve de la lista de clientes el cliente con el dni pasado, si es que existe.
	@Test
	@Tag("Cliente")
	@Tag("B�squeda")
	void testBuscarClienteExistente() {
		Cliente clienteBuscado=gestor.buscarCliente("456");
		assertEquals(cliente2, clienteBuscado);
	}
	
	// Comprobamos el m�todo buscarCliente(String dni) de la clase GestorContabilidad, que devuelve de la lista de clientes el cliente con el dni pasado. Test de un caso cuyo cliente no existe en la lista.
	@Test
	@Tag("Cliente")
	@Tag("B�squeda")
	void testBuscarClienteInexistente() {
		Cliente clienteBuscado=gestor.buscarCliente("145");
		assertNull(clienteBuscado);
	}
	
	// Comprobamos el m�todo clienteMasAntiguo() de la clase GestorContabilidad, que devuelve el cliente cuya fecha de alta es la m�s antigua.
	@Test
	@Tag("Cliente")
	@Tag("B�squeda")
	void testClienteMasAntiguo() {
		Cliente clienteBuscado=gestor.clienteMasAntiguo();
		assertEquals(cliente3, clienteBuscado);
	}
	
	// Comprobamos el m�todo clienteMasAntiguo() de la clase GestorContabilidad, que devuelve el cliente cuya fecha de alta es la m�s antigua. Test de caso err�neo.
	@Test
	@Tag("Cliente")
	@Tag("B�squeda")
	void testClienteMasAntiguoErroneo() {
		Cliente clienteBuscado=gestor.clienteMasAntiguo();
		assertNotEquals(cliente2, clienteBuscado);
	}

	// Comprobamos el m�todo eliminarCliente(dni) de la clase GestorContabilidad, que elimina un cliente, si existe, de la lista de clientes del gestor dado el dni.
	@Test
	@Tag("Cliente")
	@Tag("Eliminaci�n")
	void testEliminarClientes() {
		gestor.eliminarClientes("123");
		Cliente comprobacion=gestor.buscarCliente("123");
		assertNull(comprobacion);
	}

}
