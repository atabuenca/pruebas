package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;

class TestMetodosFacturas {
	
	private Cliente clienteEjemplo;
	private Factura facturaEjemplo;

	@BeforeEach
	void setUp() throws Exception {
		clienteEjemplo=new Cliente("Francisco", "123", LocalDate.parse("1990-08-08"));
		facturaEjemplo=new Factura("A", LocalDate.parse("1995-12-12"),"Carne", 4, 3, clienteEjemplo);
	}

	// Comprobamos el m�todo calcularPrecioTotal(float precioUnidad, int cantidad), que calcula el precio de la factura (precioUnidad * cantidad), de la clase Factura, en un caso positivo.
	@Test
	@Tag("Factura")
	@Tag("Matem�tico")
	void testCalcularPrecioTotalCasoCorrecto() {
		float precioTotal=facturaEjemplo.calcularPrecioTotal(facturaEjemplo.getPrecioUnidad(), facturaEjemplo.getCantidad());
		float precioEsperado=12;
		assertEquals(precioEsperado, precioTotal);
	}
	
	// Comprobamos el m�todo calcularPrecioTotal(float precioUnidad, int cantidad), que calcula el precio de la factura (precioUnidad * cantidad), de la clase Factura, en un caso negativo.
	@Test
	@Tag("Factura")
	@Tag("Matem�tico")
	void testCalcularPrecioTotalCasoErroneo() {
		float precioTotal=facturaEjemplo.calcularPrecioTotal(facturaEjemplo.getPrecioUnidad(), facturaEjemplo.getCantidad());
		float precioEsperado=13;
		assertNotEquals(precioEsperado, precioTotal);
	}

}
