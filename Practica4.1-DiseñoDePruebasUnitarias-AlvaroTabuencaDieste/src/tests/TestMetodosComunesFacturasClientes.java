package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

class TestMetodosComunesFacturasClientes {
	
	private static GestorContabilidad gestor;
	private static Cliente cliente1;
	private static Cliente cliente2;
	private static Cliente cliente3;
	private static Factura factura1;
	private static Factura factura2;
	private static Factura factura3;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		gestor=new GestorContabilidad();
		cliente1=new Cliente("Francisco", "123", LocalDate.parse("1990-08-08"));
		cliente2=new Cliente("Jos�", "456", LocalDate.parse("1990-07-07"));
		cliente3=new Cliente("Ramiro", "789", LocalDate.parse("1990-06-06"));
		factura1=new Factura("A", LocalDate.parse("1995-12-12"),"Carne", 5, 3, cliente1);
		factura2=new Factura("B", LocalDate.parse("1995-11-11"),"Pescado", 7, 3, cliente2);
		factura3=new Factura("C", LocalDate.parse("1995-10-10"),"Verdura", 3, 3, cliente3);
		gestor.crearFactura(factura1);
		gestor.crearFactura(factura2);
		gestor.crearFactura(factura3);
		gestor.altaCliente(cliente1);
		gestor.altaCliente(cliente2);
		gestor.altaCliente(cliente3);
		gestor.asignarClienteAFactura("123", "B");
	}

	// Comprobamos el m�todo cantidadFacturasPorCliente(String dni), que devuelve el n�mero de facturas asociadas al cliente con ese dni.
	@Test
	@Tag("Com�n")
	@Tag("Consulta")
	void testCantidadFacturasPorCliente() {
		int auxiliar=gestor.cantidadFacturasPorCliente("123");
		assertEquals(2, auxiliar);
	}
	
	// Comprobamos el m�todo cantidadFacturasPorCliente(String dni), que devuelve el n�mero de facturas asociadas al cliente con ese dni. Test de caso err�neo.
		@Test
		@Tag("Com�n")
		@Tag("Consulta")
		void testCantidadFacturasPorClienteErroneo() {
			int auxiliar=gestor.cantidadFacturasPorCliente("123");
			assertNotEquals(1, auxiliar);
		}

}
