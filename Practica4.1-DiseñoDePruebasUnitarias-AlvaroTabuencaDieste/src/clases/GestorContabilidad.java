package clases;

import java.time.LocalDate;
import java.util.ArrayList;

public class GestorContabilidad {
	
	private ArrayList<Factura> listaFacturas;
	private ArrayList<Cliente> listaClientes;
	
	public GestorContabilidad()
	{
		listaFacturas=new ArrayList<Factura>();
		listaClientes=new ArrayList<Cliente>();
	}

	public ArrayList<Factura> getListaFacturas() 
	{
		return listaFacturas;
	}

	public ArrayList<Cliente> getListaClientes() 
	{
		return listaClientes;
	}
	
	public Cliente buscarCliente(String dni)
	{
		for(Cliente clientes : listaClientes)
		{
			if(clientes.getDni().equals(dni))
			{
				return clientes;
			}
		}
		
		return null;
	}
	
	public Factura buscarFactura(String codigo)
	{
		for(Factura facturas : listaFacturas)
		{
			if(facturas.getCodigoFactura().equals(codigo))
			{
				return facturas;
			}
		}
		
		return null;
	}
	
	public void altaCliente(Cliente cliente)
	{
		int contador=0;
		
		for(Cliente clientes : listaClientes)
		{
			if(clientes.getDni().equals(cliente.getDni()))
			{
				contador++;
			}
		}
		
		if(contador==0)
		{
			listaClientes.add(cliente);
		}
		
		else
		{
			System.out.println("Ya existe un cliente en la lista con ese DNI. No se ha podido a�adir a la lista de clientes.");
		}
	}
	
	public void crearFactura(Factura factura)
	{
		int contador=0;
		
		for(Factura facturas : listaFacturas)
		{
			if(facturas.getCodigoFactura().equals(factura.getCodigoFactura()))
			{
				contador++;
			}
		}
		
		if(contador==0)
		{
			listaFacturas.add(factura);
		}
		
		else
		{
			System.out.println("Ya existe una factura con ese c�digo. No se ha podido a�adir a la lista de facturas.");
		}
	}
	
	public Cliente clienteMasAntiguo()
	{
		LocalDate fechaMasAntigua=LocalDate.now();
		
		for(Cliente clientes: listaClientes)
		{
			if(clientes.getFechaAlta().isBefore(fechaMasAntigua))
			{
				fechaMasAntigua=clientes.getFechaAlta();
			}
		}
		
		if(fechaMasAntigua.isBefore(LocalDate.now()))
		{
			for(Cliente clientes : listaClientes)
			{
				if(clientes.getFechaAlta().equals(fechaMasAntigua))
				{
					return clientes;
				}
			}
		}
		
		return null;
	}
	
	public Factura facturaMasCara()
	{
		float precioMasCaro=0;
		
		for(Factura facturas : listaFacturas)
		{
			if(precioMasCaro<facturas.calcularPrecioTotal(facturas.getPrecioUnidad(), facturas.getCantidad()))
			{
				precioMasCaro=facturas.calcularPrecioTotal(facturas.getPrecioUnidad(), facturas.getCantidad());
			}
		}
		
		if(precioMasCaro>0)
		{
			for(Factura facturas : listaFacturas)
			{
				if(facturas.calcularPrecioTotal(facturas.getPrecioUnidad(), facturas.getCantidad())==precioMasCaro)
				{
					return facturas;
				}
			}
		}
		
		return null;
	}
	
	public float calcularFacturacionAnual(int anno)
	{
		float facturacionTotalAnno=0;
		
		for(Factura facturas : listaFacturas)
		{
			if(facturas.getFecha().getYear()==anno)
			{
				facturacionTotalAnno=facturacionTotalAnno+facturas.calcularPrecioTotal(facturas.getPrecioUnidad(), facturas.getCantidad());
			}
		}
		
		return facturacionTotalAnno;
	}
	
	public void asignarClienteAFactura(String dni, String codigoFactura)
	{
		for(Factura facturas : listaFacturas)
		{
			if(facturas.getCodigoFactura().equals(codigoFactura))
			{
				for(Cliente clientes : listaClientes)
				{
					if(clientes.getDni().equals(dni))
					{
						facturas.setClienteFactura(clientes);
					}
				}
			}
		}
	}
	
	public int cantidadFacturasPorCliente(String dni)
	{
		int contador=0;
		
		for(Factura facturas : listaFacturas)
		{
			if(facturas.getClienteFactura().getDni().equals(dni))
			{
				contador++;
			}
		}
		
		return contador;
	}
	
	public void eliminarFactura(String codigo)
	{
		for(Factura facturas : listaFacturas)
		{
			if(facturas.getCodigoFactura().equals(codigo))
			{
				listaFacturas.remove(facturas);
			}
		}
	}
	
	public void eliminarClientes(String dni)
	{
		int auxiliar=cantidadFacturasPorCliente(dni);
		Cliente clienteBorrar = null;
		
		for(Cliente clientes : listaClientes)
		{
			if((clientes.getDni().equals(dni)) && (auxiliar==0))
			{
				clienteBorrar=clientes;
			}
		}
		
		listaClientes.remove(clienteBorrar);
	}
	
}
