package clases;

import java.time.LocalDate;

public class Factura {
	
	private String codigoFactura;
	private LocalDate fecha;
	private String nombreProducto;
	private float precioUnidad;
	private int cantidad;
	private Cliente clienteFactura;
	
	public Factura(String codigoPasado, LocalDate fechaPasada, String nombrePasado, float precioPasado, int cantidadPasada, Cliente clientePasado)
	{
		codigoFactura=codigoPasado;
		fecha=fechaPasada;
		nombreProducto=nombrePasado;
		precioUnidad=precioPasado;
		cantidad=cantidadPasada;
		clienteFactura=clientePasado;
	}
	
	public String getCodigoFactura() 
	{
		return codigoFactura;
	}
	
	public void setCodigoFactura(String codigoFactura) 
	{
		this.codigoFactura = codigoFactura;
	}
	
	public LocalDate getFecha() 
	{
		return fecha;
	}
	
	public void setFecha(LocalDate fecha) 
	{
		this.fecha = fecha;
	}
	
	public String getNombreProducto() 
	{
		return nombreProducto;
	}
	
	public void setNombreProducto(String nombreProducto) 
	{
		this.nombreProducto = nombreProducto;
	}
	
	public float getPrecioUnidad() 
	{
		return precioUnidad;
	}
	
	public void setPrecioUnidad(float precioUnidad) 
	{
		this.precioUnidad = precioUnidad;
	}
	
	public int getCantidad() 
	{
		return cantidad;
	}
	
	public void setCantidad(int cantidad) 
	{
		this.cantidad = cantidad;
	}
	
	public Cliente getClienteFactura() 
	{
		return clienteFactura;
	}
	
	public void setClienteFactura(Cliente clienteFactura) 
	{
		this.clienteFactura = clienteFactura;
	}
	
	public float calcularPrecioTotal(float precioUnidad, int cantidad)
	{
		float precioTotal=precioUnidad*(float)cantidad;
		return precioTotal;
	}

	
	@Override
	public String toString() {
		String cadena=codigoFactura+" "+fecha+" "+nombreProducto+" "+precioUnidad+" "+cantidad+" "+clienteFactura;
		return cadena;
	}
	
}
